# Simple ASCII-based Hangman server and client

## build instructions

`make && make client`

## start server
`./server`

## start client
`./client`

## build parallel server using select
`make selectserver`

## start select server
`./selectserver`