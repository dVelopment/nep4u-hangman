/* 
 * File:   main.c
 * Author: daniel
 *
 * Created on 16. März 2013, 11:53
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <signal.h>
#include "base.h"

extern void ServerProcess (int in, int out);

/**
 * waiter accepts the exit code from a child created previously
 */
void waiter() {
    int cpid, stat;
    
    cpid = wait (&stat); /* wait for a child to terminate */
    signal (SIGCHLD, waiter); /* reinstall signal handler */
}

/*
 * 
 */
int main(int argc, char** argv) {
    signal (SIGCHLD, waiter); /* install signal handler */
    
    int my_sock;
    
    my_sock = socket(AF_INET, SOCK_STREAM, 0);
    
    if (my_sock < 0) {
        perror("cannot get socket");
        exit(1);
    }
    
    int opt = 1;
    setsockopt(my_sock, SOL_SOCKET, SO_REUSEADDR, (char *) &opt, sizeof(opt));

    struct sockaddr_in server;
    
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons(SERVER_PORT);
    
    if (bind(my_sock, (struct sockaddr *) & server, sizeof(server)) < 0) {
        perror("cannot bind");
        exit(1);
    }
    
    if (listen(my_sock, 5) < 0) {
        perror("listening failed");
        exit(1);
    }
    printf("listening on port %d\n", SERVER_PORT);
    
    struct sockaddr_in client;
    int fd, client_len;
    
    client_len = sizeof(client);
    
    while (1) {
        fd = accept(my_sock, &client, &client_len);
        
        if (fd < 0) {
            perror("accepting failed\n");
            exit(1);
        }
        printf("client connected\n");
        /* client connected => create child process */
        int pid = fork();

        if (pid == 0) {
            /* child process */
            ServerProcess(fd, fd);

            close(fd);
            printf("connection closed\n");
            exit(0);
        } else if (pid < 0) {
            perror("forking failed\n");
            exit(1);
        } else {
            /* parent process */
            printf("parent process close\n");
            close(fd);
        }

    }
    
    close(my_sock);
    return 0;
}

