CC      = gcc
CCFLAGS = -ansi -g -w

LD      = gcc
LDFLAGS = -lm

CFILESSERVER = WordCheck.c server.c
OFILESSERVER = $(CFILESSERVER:.c=.o)
	
CFILESCLIENT = client.c
OFILESCLIENT = $(CFILESCLIENT:.c=.o)
	
CFILESSELECTSERVER = service.c select_server.c
OFILESSELECTSERVER = $(CFILESSELECTSERVER:.c=.o)

.c.o:
	$(CC) $(CCFLAGS) -c $<

hangmanserver: $(OFILESSERVER)
	$(LD) -o $@ $(OFILESSERVER) $(LDFLAGS)
	
selectserver: $(CFILESSELECTSERVER)
	$(LD) -o $@ $(CFILESSELECTSERVER) $(LDFLAGS)
	
client:	$(OFILESCLIENT)
	$(LD) -o $@ $(OFILESCLIENT) $(LDFLAGS)

clean:
	rm -f $(OFILESSERVER) $(OFILESCLIENT)

depend:
	makedepend $(CFILESSERVER)
	makedepend $(CFILESCLIENT)

# DO NOT DELETE

client.o: /usr/include/stdio.h /usr/include/sys/cdefs.h
client.o: /usr/include/sys/_symbol_aliasing.h
client.o: /usr/include/sys/_posix_availability.h /usr/include/Availability.h
client.o: /usr/include/AvailabilityInternal.h /usr/include/_types.h
client.o: /usr/include/sys/_types.h /usr/include/machine/_types.h
client.o: /usr/include/i386/_types.h /usr/include/secure/_stdio.h
client.o: /usr/include/secure/_common.h /usr/include/stdlib.h
client.o: /usr/include/sys/wait.h /usr/include/sys/signal.h
client.o: /usr/include/sys/appleapiopts.h /usr/include/machine/signal.h
client.o: /usr/include/i386/signal.h /usr/include/i386/_structs.h
client.o: /usr/include/sys/_structs.h /usr/include/machine/_structs.h
client.o: /usr/include/sys/resource.h /usr/include/machine/endian.h
client.o: /usr/include/i386/endian.h /usr/include/sys/_endian.h
client.o: /usr/include/libkern/_OSByteOrder.h
client.o: /usr/include/libkern/i386/_OSByteOrder.h /usr/include/alloca.h
client.o: /usr/include/machine/types.h /usr/include/i386/types.h
