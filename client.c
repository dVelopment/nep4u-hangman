/* 
 * File:   client.c
 * Author: daniel
 *
 * Created on 18. März 2013, 21:43
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <poll.h>
#include <unistd.h>

#include "base.h"
#define BUFFER_SIZE 80
#define SERVER_HANDLE 0
#define LOCAL_HANDLE 1

typedef enum bool {false, true} bool;

bool gameOver(char *buffer) {
    return strstr(buffer, "Game over") != NULL || strstr(buffer, "You won!") != NULL;
}

int main(int argc, char** argv) {
    int mySocket;
    
    mySocket = socket(AF_INET, SOCK_STREAM, 0);
    
    if (mySocket < 0) {
        perror("cannot allocate socket");
        exit(1);
    }
    
    struct sockaddr_in server_addr;
    struct hostent *hp;

    if ((hp = gethostbyname("127.0.0.1")) == NULL) {
        perror("unknown host");
        exit(1);
    }
    
    bcopy(hp->h_addr, &server_addr.sin_addr, hp->h_length);
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(SERVER_PORT);
    
    if (connect(mySocket, (struct sockaddr *) &server_addr, sizeof(server_addr)) < 0) {
        perror("cannot connect to server");
        exit(1);
    }
    
    printf("connected to server on port: %d\n", SERVER_PORT);
    
    struct pollfd pfd[2];
    pfd[LOCAL_HANDLE].fd = 0; /* terminal */
    pfd[LOCAL_HANDLE].events = POLLIN;
    pfd[SERVER_HANDLE].fd = mySocket; /* network */
    pfd[SERVER_HANDLE].events = POLLIN;
    
    char buffer[BUFFER_SIZE];
    bzero(buffer, BUFFER_SIZE);
    
    while (true) {
        int numberOfFds;
        /* wait for events to occurr */
        if (numberOfFds = poll(pfd, 2, -1) < 0) {
            perror("poll failed\n");
            exit(1);
        }
        
        /* check each handle */
        int i, n;
        for (i = 0; i <= LOCAL_HANDLE; i++) {
            /* check if an error occurred */
            if (pfd[i].revents&(POLLERR|POLLHUP|POLLNVAL)) {
                return; /* just return */
            }
            
            /* if there's data to read, 
             * read it and put it out on the correct fd
             */
            if (pfd[i].revents&POLLIN) {
                n = read(pfd[i].fd, buffer, BUFFER_SIZE);
                
                if (n > 0) {
                    if (i == SERVER_HANDLE) {
                        /* write on stdout */
                        write(1, buffer, n);
                        
                        /* check game status */
                        if (gameOver(buffer)) {
                            close(mySocket);
                            exit(0);
                        }
                    } else {
                        /* write on socket */
                        write(mySocket, buffer, 1);
                    }
                }
            }
        }
    }
    close(mySocket);

    return 0;
}

