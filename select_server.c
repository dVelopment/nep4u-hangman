/* 
 * File:   main.c
 * Author: daniel
 *
 * Created on 16. März 2013, 11:53
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdlib.h>
#include "base.h"

#include "service.h"

/*
 * 
 */
int main(int argc, char** argv) {
    int my_sock;
    
    my_sock = socket(AF_INET, SOCK_STREAM, 0);
    
    if (my_sock < 0) {
        perror("cannot get socket");
        exit(1);
    }
    
    int opt = 1;
    setsockopt(my_sock, SOL_SOCKET, SO_REUSEADDR, (char *) &opt, sizeof(opt));

    struct sockaddr_in server;
    
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons(SERVER_PORT);
    
    if (bind(my_sock, (struct sockaddr *) & server, sizeof(server)) < 0) {
        perror("cannot bind");
        exit(1);
    }
    
    if (listen(my_sock, 5) < 0) {
        perror("listening failed");
        exit(1);
    }
    printf("listening on port %d\n", SERVER_PORT);
    
    struct sockaddr_in client;
    int fd, client_len;
    
    client_len = sizeof(client);
    
    fd_set master; /* master file descriptor list */
    fd_set read_fds; /* tmp file descr. list for select() */
    int fdmax; /* maximum file descriptor number */
    
    int newfd; /* newly accepted socket descriptor */
    char buffer[256]; /* buffer for client data */
    int nbytes;
    
    /* prepare master and read_fds (empty them) */
    FD_ZERO(&master);
    FD_ZERO(&read_fds);
    
    /* add my_sock to master set */
    FD_SET(my_sock, &master);
    
    /* keep track of the biggest file descriptor */
    fdmax = my_sock; /* so far it's the only one */
    
    while (1) {
        /* copy master set */
        read_fds = master;
        
        if (select(fdmax+1, &read_fds, NULL, NULL, NULL) < 0) {
            perror("select failed\n");
            exit(1);
        }
        
        /* search connection */
        int i;
        for (i = 0; i <= fdmax; i++) {
            if (FD_ISSET(i, &read_fds)) {
                /* found one */
                if (i == my_sock) {
                    /* handle new connections */
                    newfd = accept(my_sock, &client, &client_len);

                    if (newfd < 0) {
                        perror("accept failed\n");
                        exit(1);
                    }
                    
                    /* add it to master */
                    FD_SET(newfd, &master);
                    if (newfd > fdmax) {
                        fdmax = newfd;
                    }
                    service_init(newfd);
                } else {
                    /* handle data from a client */
                    if (service_do(i) == 0) {
                        service_exit(i);
                        FD_CLR(i, &master);
                    }
                }
                
            }
        }
    }
    

    return 0;
}

